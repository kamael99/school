# School Application

## Требования к сборке

1. JDK 18.0.2
2. Maven 3.8.6
3. Docker

## Запуск БД и веб-приложения

Запуск БД
```
docker run -p 5433:5432 --name schoolContainer -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=sc -d postgres
```
Остановить контейнер с БД
```
docker stop schoolContainer
```
Удалить контейнер с БД
```
docker rm schoolContainer
```

Далее собираем и запускаем веб-приложение
```
docker build -t schoolapp .  
docker run -p 8081:8081 schoolapp
```
Остановить контейнер
```
docker stop <имя контейнера>
```

URL
```
localhost:8081
```