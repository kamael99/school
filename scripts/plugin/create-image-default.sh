##!/bin/bash

pushd ../../

./mvnw spring-boot:build-image -DskipTests=true -Dspring-boot.build-image.imageName=danil/default-image

# get image info
docker image ls | grep $IMAGE_NAME