--changeset liquibase:1
CREATE TABLE academicyear
(
    id    SERIAL PRIMARY KEY,
    title INTEGER NOT NULL
);
--changeset liquibase:2
CREATE TABLE grade
(
    id    SERIAL PRIMARY KEY,
    title VARCHAR(50) NOT NULL
);
--changeset liquibase:3
CREATE TABLE parallel
(
    id    SERIAL PRIMARY KEY,
    title INTEGER NOT NULL,
    grade_id INTEGER REFERENCES grade (id) NOT NULL
);
--changeset liquibase:4
CREATE TABLE teacher
(
    id        SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    surname VARCHAR(100) NOT NULL,
    patronymic VARCHAR(100) NOT NULL
);
--changeset liquibase:5
CREATE TABLE edclass
(
    id       SERIAL PRIMARY KEY,
    year_id  INTEGER REFERENCES academicyear (id) NOT NULL,
    prl_id  INTEGER REFERENCES parallel (id) NOT NULL,
    teach_id INTEGER REFERENCES teacher (id) NOT NULL
);
--changeset liquibase:6
CREATE TABLE students
(
    id          SERIAL PRIMARY KEY,
    class_id    INTEGER REFERENCES edclass (id) NOT NULL,
    delete_date TIMESTAMP,
    name VARCHAR(100) NOT NULL,
    surname VARCHAR(100) NOT NULL,
    patronymic VARCHAR(100) NOT NULL

);
--changeset liquibase:7
INSERT INTO academicyear(title)
values (2012),
       (2013),
       (2014),
       (2015),
       (2016),
       (2017),
       (2018),
       (2019),
       (2020),
       (2021),
       (2022),
       (2023);
--changeset liquibase:8
INSERT INTO grade(title)
values ('начальная'),
       ('средняя'),
       ('старшая');
--changeset liquibase:9
INSERT INTO parallel(title, grade_id)
values (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 2),
       (6, 2),
       (7, 2),
       (8, 2),
       (9, 2),
       (10, 3),
       (11, 3);

--changeset liquibase:10
CREATE TABLE users
(
    id SERIAL PRIMARY KEY,
    login text,
    password text,
    name text,
    surname text,
    patronymic text,
    is_active boolean,
    is_admin boolean,
    datetime_of_creation TIMESTAMP,
    datetime_of_delete TIMESTAMP
);
--changeset liquibase:11
INSERT INTO teacher (surname, name, patronymic) values ('Максимов', 'Николай', 'Денисович');
INSERT INTO teacher (surname, name, patronymic) values ('Артемова', 'Яна', 'Михайловна');
INSERT INTO teacher (surname, name, patronymic) values ('Смирнова', 'Виктория', 'Тимуровна');
INSERT INTO teacher (surname, name, patronymic) values ('Горячев', 'Демид', 'Александрович');
INSERT INTO teacher (surname, name, patronymic) values ('Кузнецов', 'Михаил', 'Александрович');
INSERT INTO teacher (surname, name, patronymic) values ('Петров', 'Ярослав', 'Маркович');
INSERT INTO teacher (surname, name, patronymic) values ('Львов', 'Роман', 'Давидович');
INSERT INTO teacher (surname, name, patronymic) values ('Филиппова', 'Майя', 'Захаровна');
INSERT INTO teacher (surname, name, patronymic) values ('Борисова', 'Алиса', 'Ивановна');
INSERT INTO teacher (surname, name, patronymic) values ('Захарова', 'София', 'Тимофеевна');
INSERT INTO teacher (surname, name, patronymic) values ('Васильев', 'Фёдор', 'Михайлович');

--changeset liquibase:12
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 1, 1);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 2, 2);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 3, 3);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 4, 4);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 5, 5);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 6, 6);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 7, 7);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 8, 8);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 9, 9);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 10, 10);
INSERT INTO edclass (year_id, prl_id, teach_id) values (1, 11, 11);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 1, 1);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 2, 2);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 3, 3);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 4, 4);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 5, 5);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 6, 6);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 7, 7);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 8, 8);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 9, 9);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 10, 10);
INSERT INTO edclass (year_id, prl_id, teach_id) values (2, 11, 11);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 1, 1);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 2, 2);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 3, 3);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 4, 4);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 5, 5);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 6, 6);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 7, 7);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 8, 8);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 9, 9);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 10, 10);
INSERT INTO edclass (year_id, prl_id, teach_id) values (3, 11, 11);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 1, 1);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 2, 2);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 3, 3);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 4, 4);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 5, 5);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 6, 6);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 7, 7);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 8, 8);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 9, 9);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 10, 10);
INSERT INTO edclass (year_id, prl_id, teach_id) values (4, 11, 11);

--changeset liquibase:13
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Зуева', 'Александра', 'Тимуровна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Глебов', 'Григорий', 'Андреевич');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Григорьева', 'Диана', 'Ивановна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Филатов', 'Егор', 'Давидович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Савельева', 'Мария', 'Макаровна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Фомин', 'Михаил', 'Иванович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Михайлов', 'Алексей', 'Елисеевич');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Левина', 'Виктория', 'Павловна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Нефедова', 'Анна', 'Михайловна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (1, null, 'Лукьянов', 'Николай', 'Михайлович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Корнилова', 'Стефания', 'Арсентьевна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Морозова', 'София', 'Ярославовна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Крылов', 'Владимир', 'Лукич');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Смирнова', 'Вера', 'Никитична');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Фетисова', 'Анна', 'Львовна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Плотникова', 'Наталья', 'Артёмовна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Львов', 'Дмитрий', 'Дмитриевич');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Прокофьева', 'София', 'Леонидовна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Горелов', 'Георгий', 'Маркович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (2, null, 'Кириллова', 'Полина', 'Гордеевна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null, 'Максимов', 'Руслан', 'Давидович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Поляков', 'Иван', 'Вячеславович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Морозова', 'Кристина', 'Глебовна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Кузнецов', 'Владимир', 'Даниилович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Соловьев', 'Алексей', 'Маркович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Иванов', 'Артемий', 'Максимович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Соболев', 'Гордей', 'Артурович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Кузьмин', 'Тимур', 'Богданович');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Фролова', 'Елизавета', 'Алиевна');
INSERT INTO students (class_id, delete_date, surname, name, patronymic) values (3, null,'Ширяева', 'Мелания', 'Кирилловна');

--changeset liquibase:14
INSERT INTO users (login, password, name, surname, patronymic, is_active, is_admin, datetime_of_creation, datetime_of_delete) values ('danil','$2a$10$bMCGRoJtSq7gNHWH96CnJuWjJlgzQkm9gdGktca1iXZomYLV8G3mi','danil','danil','danil',true,true, current_timestamp, null);