package com.example.school.Entities.Parallel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/parallel")
public class ParallelController {

    private final ParallelRepository parallelRepository;

    @Autowired
    public ParallelController(ParallelRepository parallelRepository) { this.parallelRepository = parallelRepository;}

    @GetMapping("/all")
    public List<ParallelDTO> getAll() {
        return parallelRepository.findAll();
    }
}
