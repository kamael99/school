package com.example.school.Entities.Parallel;

import lombok.Data;

@Data
public class ParallelDTO {
    private Integer id;
    private String title;
    private Integer gradeId;
}
