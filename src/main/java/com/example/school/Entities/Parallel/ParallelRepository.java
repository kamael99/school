package com.example.school.Entities.Parallel;

import com.example.school.jooq.Tables;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
@RequiredArgsConstructor
public class ParallelRepository {
    @Autowired
    private final DSLContext dsl;

    public ParallelDTO find(Integer id) {
        return dsl.selectFrom(Tables.PARALLEL)
                .where(Tables.PARALLEL.ID.eq(id))
                .fetchOneInto(ParallelDTO.class);
    }

    public List<ParallelDTO> findAll() {
        return dsl.selectFrom(Tables.PARALLEL)
                .fetch()
                .into(ParallelDTO.class);
    }
}
