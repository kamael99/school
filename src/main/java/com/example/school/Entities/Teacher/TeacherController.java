package com.example.school.Entities.Teacher;

import com.example.school.Exceptions.CustomException;
import com.example.school.jooq.Tables;
import com.example.school.jooq.tables.pojos.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;


    @PostMapping("/new")
    public ResponseEntity<TeacherDTO> createTeacher(@RequestBody Teacher teacher) throws CustomException {
        TeacherDTO t = teacherService.create(teacher);
        return ResponseEntity.ok(t);
    }

    @GetMapping("/")
    public ResponseEntity<TeacherResponseDTO> getTeachers(@RequestParam(required = false, defaultValue = "50") Integer pageSize,
                                                          @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {


        TeacherResponseDTO teacherResponseDTO = teacherService.getTeachers(pageSize, pageNumber);
        return ResponseEntity.ok(teacherResponseDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TeacherDTO> getTeacher(@PathVariable(value = "id") Integer id) throws CustomException {
        TeacherDTO t = teacherService.getTeacher(id);
        return ResponseEntity.ok(t);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TeacherDTO> updateTeacher(@PathVariable(value = "id") Integer id,
                                                    @RequestBody Teacher teacher) throws CustomException {
        TeacherDTO t = teacherService.update(id, teacher);
        return ResponseEntity.ok(t);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteTeacher(@PathVariable(value = "id") Integer id) throws CustomException {

        Boolean b = teacherService.delete(id);
        if (b)
            return ResponseEntity.ok("OK");
        else
            return ResponseEntity.badRequest().body("Error");
    }
}
