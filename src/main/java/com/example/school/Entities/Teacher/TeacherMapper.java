package com.example.school.Entities.Teacher;

import org.springframework.stereotype.Service;
import com.example.school.jooq.tables.pojos.Teacher;

import java.util.List;

@Service
public class TeacherMapper {


    public TeacherDTO mapToTeacherDTO(Teacher teacher) {
        TeacherDTO dto = new TeacherDTO();

        dto.setId(teacher.getId());
        dto.setName(teacher.getName());
        dto.setSurname(teacher.getSurname());
        dto.setPatronymic(teacher.getPatronymic());
        return dto;
    }

    public TeacherResponseDTO mapToTeacherResponseDTO(List<TeacherDTO> teacherDTOList, Integer currentPage, Integer totalItems) {
        TeacherResponseDTO teacherResponseDTO = new TeacherResponseDTO();
        teacherResponseDTO.setTeacherDTOList(teacherDTOList);
        teacherResponseDTO.setPageCount((int)Math.ceil(totalItems/15.0));
        teacherResponseDTO.setTotalItems(totalItems);
        return teacherResponseDTO;
    }
}
