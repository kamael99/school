package com.example.school.Entities.Teacher;

import com.example.school.jooq.Tables;
import com.example.school.jooq.tables.pojos.Teacher;
import liquibase.repackaged.net.sf.jsqlparser.statement.select.OrderByElement;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.jaxb.SpringDataJaxb;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class TeacherRepository {

    @Autowired
    private final DSLContext dsl;

    private final TeacherMapper teacherMapper;


    public TeacherDTO insert(Teacher teacher) {
        return dsl.insertInto(Tables.TEACHER)
                .set(dsl.newRecord(Tables.TEACHER, teacher))
                .returning()
                .fetchOneInto(TeacherDTO.class);
    }

    public TeacherDTO find(Integer id) {
        return dsl.selectFrom(Tables.TEACHER)
                .where(Tables.TEACHER.ID.eq(id))
                .fetchOneInto(TeacherDTO.class);
    }

    public List<TeacherDTO> findAll(Integer pageSize, Integer pageNumber) {
        return dsl.selectFrom(Tables.TEACHER)
                .orderBy(Tables.TEACHER.ID.asc())
                .limit(pageSize)
                .offset(pageNumber * pageSize)
                .fetchInto(Teacher.class)
                .stream().map(teacherMapper::mapToTeacherDTO)
                .collect(Collectors.toList());
    }


    public Integer getTotalItems() {
        return dsl.selectCount()
                .from(Tables.TEACHER)
                //.where(condition)
                .fetchOneInto(Integer.class);
    }


    public TeacherDTO update(Integer id, Teacher teacher) {
        return dsl.update(Tables.TEACHER)
                .set(dsl.newRecord(Tables.TEACHER, teacher))
                .where(Tables.TEACHER.ID.eq(id))
                .returning()
                .fetchOneInto(TeacherDTO.class);
    }

    public Boolean delete(Integer id) {
        return dsl.deleteFrom(Tables.TEACHER)
                .where(Tables.TEACHER.ID.eq(id))
                .andNotExists(dsl.selectFrom(Tables.EDCLASS).where(Tables.EDCLASS.TEACH_ID.eq(id)))
                .execute() == 1;
    }


}
