package com.example.school.Entities.Teacher;

import lombok.Data;

@Data
public class TeacherDTO {

    private Integer id;

    private String name;

    private String surname;

    private String patronymic;
}
