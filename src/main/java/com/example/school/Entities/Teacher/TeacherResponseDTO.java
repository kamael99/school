package com.example.school.Entities.Teacher;

import lombok.Data;

import java.util.List;

@Data
public class TeacherResponseDTO {

    private Integer totalItems;
    private Integer pageCount;
    private List<TeacherDTO> teacherDTOList;
}
