package com.example.school.Entities.Teacher;

import com.example.school.Exceptions.CustomException;
import com.example.school.jooq.Tables;
import com.example.school.jooq.tables.pojos.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@Service
public class TeacherService {

    private final Integer defaultPageSize = 15;

    private final TeacherMapper teacherMapper = new TeacherMapper();
    @Autowired
    private TeacherRepository teacherRepository;

    public TeacherDTO create(Teacher teacher) throws CustomException {
        // Если передали id, то возвращаем ошибку
        if (teacher.getId() != null)
            throw new CustomException("Id field is prohibited", HttpStatus.BAD_REQUEST);
        // Пустое имя
        if (teacher.getName() == null || teacher.getName().isBlank())
            throw new CustomException("Missing required field: name", HttpStatus.BAD_REQUEST);
        //Пустая фамилия, допускаем, что отчества может не быть
        if (teacher.getSurname() == null || teacher.getSurname().isBlank())
            throw new CustomException("Missing required field: surname", HttpStatus.BAD_REQUEST);
        return teacherRepository.insert(teacher);
    }

    public TeacherDTO getTeacher(Integer id) throws CustomException {
        TeacherDTO t = teacherRepository.find(id);
        // Нет сущности с таким id
        if (t == null)
            throw new CustomException(String.format("Teacher with id %d not found", id), HttpStatus.NOT_FOUND);
        return t;
    }

    public TeacherResponseDTO getTeachers(Integer pageSize, Integer pageNumber) {
        if (pageSize > defaultPageSize)
            pageSize = defaultPageSize;

        return teacherMapper.mapToTeacherResponseDTO(teacherRepository.findAll(pageSize, pageNumber),
                pageNumber,
                teacherRepository.getTotalItems());
    }

    public TeacherDTO update(Integer id, Teacher teacher) throws CustomException {
        TeacherDTO t = teacherRepository.update(id, teacher);
        // Пустое имя
        if (teacher.getName().isBlank())
            throw new CustomException("Name can't be empty", HttpStatus.BAD_REQUEST);
        // Пустая фамилия
        if (teacher.getSurname().isBlank())
            throw new CustomException("Surname can't be empty", HttpStatus.BAD_REQUEST);
        // Нет сущности с таким id
        if (t == null)
            throw new CustomException(String.format("Teacher with id %d not found", id), HttpStatus.NOT_FOUND);

        return t;
    }

    public Boolean delete(Integer id) throws CustomException {
        Boolean b = teacherRepository.delete(id);
        // Нет сущности с таким id
        if (!b)
            throw new CustomException(String.format("Teacher with id %d not found or has EdClasses", id), HttpStatus.NOT_FOUND);
        return b;
    }
}
