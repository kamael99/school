package com.example.school.Entities.EdClass;

import com.example.school.Entities.AcademicYear.AcademicYearRepository;
import com.example.school.Entities.Parallel.ParallelRepository;
import com.example.school.Entities.Teacher.TeacherRepository;
import com.example.school.Exceptions.CustomException;
import com.example.school.jooq.tables.pojos.Edclass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class EdClassService {

    @Autowired
    private EdClassRepository edClassRepository;
    @Autowired
    private AcademicYearRepository academicYearRepository;

    @Autowired
    private ParallelRepository parallelRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private EdClassMapper edClassMapper;

    private final Integer defaultPageSize = 15;

    public EdClassDTO create(Edclass edClass) throws CustomException {
        // Если передали id, то возвращаем ошибку
        if (edClass.getId() != null)
            throw new CustomException("Id field is prohibited", HttpStatus.BAD_REQUEST);
        // Отсутствует поле year_id
        if (edClass.getYearId() == null)
            throw new CustomException("Missing required field: yearId", HttpStatus.BAD_REQUEST);
        // Отсутствует поле prl_id
        if (edClass.getPrlId() == null)
            throw new CustomException("Missing required field: prlId", HttpStatus.BAD_REQUEST);
        // Отсутствует поле teach_id
        if (edClass.getTeachId() == null)
            throw new CustomException("Missing required field: teachId", HttpStatus.BAD_REQUEST);
        // Не существует год с таким id
        if (academicYearRepository.find(edClass.getYearId()) == null)
            throw new CustomException(String.format("AcademicYear with id %d not found", edClass.getYearId()), HttpStatus.BAD_REQUEST);
        // Не существует параллель с таким id
        if (parallelRepository.find(edClass.getPrlId()) == null)
            throw new CustomException(String.format("Parallel with id %d not found", edClass.getPrlId()), HttpStatus.BAD_REQUEST);
        // Не существует учитель с таким id
        if (teacherRepository.find(edClass.getTeachId()) == null)
            throw new CustomException(String.format("Teacher with id %d not found", edClass.getPrlId()), HttpStatus.BAD_REQUEST);

        return edClassRepository.insert(edClass);
    }

    public EdClassResponseDTO getEdClasses(Integer pageSize, Integer pageNumber) {
        if (pageSize > defaultPageSize)
            pageSize = defaultPageSize;

        return edClassMapper.mapToEdClassResponseDTO(edClassRepository.findAll(pageSize, pageNumber),
                pageNumber,
                edClassRepository.getTotalItems());
    }

    public EdClassDTO getEdClass(Integer id) throws CustomException {
        EdClassDTO e = edClassRepository.find(id);
        // Не существует класса с таким id
        if (e == null)
            throw new CustomException(String.format("EdClass with id %d not found", id), HttpStatus.NOT_FOUND);
        return e;
    }

    public EdClassDTO update(Integer id, Edclass edClass) throws CustomException {
        // Год не сущесвует
        if (edClass.getYearId() != null && academicYearRepository.find(edClass.getYearId()) == null)
            throw new CustomException(String.format("Academicyear with id %d not found", edClass.getYearId()), HttpStatus.BAD_REQUEST);
        // Параллель не существует
        if (edClass.getPrlId() != null && parallelRepository.find(edClass.getPrlId()) == null)
            throw new CustomException(String.format("Parallel with id %d not found", edClass.getPrlId()), HttpStatus.BAD_REQUEST);
        // Учитель не существует
        if (edClass.getTeachId() != null && teacherRepository.find(edClass.getTeachId()) == null)
            throw new CustomException(String.format("Teacher with id %d not found", edClass.getTeachId()), HttpStatus.BAD_REQUEST);
        EdClassDTO e = edClassRepository.update(id, edClass);
        // Нет сущности с таким id
        if (e == null)
            throw new CustomException(String.format("EdClass with id %d not found", id), HttpStatus.NOT_FOUND);

        return e;

    }

    public Boolean delete(Integer id) throws CustomException {
        Boolean b = edClassRepository.delete(id);
        if (!b)
            throw new CustomException(String.format("EdClass with id %d is not empty", id), HttpStatus.CONFLICT);
        return b;
    }

}
