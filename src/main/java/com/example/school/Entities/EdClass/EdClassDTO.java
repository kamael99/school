package com.example.school.Entities.EdClass;

import lombok.Data;
import com.example.school.jooq.tables.pojos.Academicyear;
import com.example.school.jooq.tables.pojos.Parallel;
import com.example.school.jooq.tables.pojos.Teacher;


@Data
public class EdClassDTO {
    private Integer id;
    private Integer yearId;
    private Integer prlId;
    private Integer teachId;

    private Academicyear academicYear;
    private Parallel parallel;
    private Teacher teacher;
}
