package com.example.school.Entities.EdClass;

import com.example.school.Exceptions.CustomException;
import com.example.school.jooq.tables.pojos.Edclass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/edclass")
public class EdClassController {

    @Autowired
    private EdClassService edClassService;

    @PostMapping("/new")
    public ResponseEntity<EdClassDTO> createEdClass(@RequestBody Edclass edClass) throws CustomException {
        EdClassDTO e = edClassService.create(edClass);
        return ResponseEntity.ok(e);
    }

    @GetMapping("/")
    public ResponseEntity<EdClassResponseDTO> getEdClasses(@RequestParam(required = false, defaultValue = "50") Integer pageSize,
                                                           @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        EdClassResponseDTO edClassResponseDTO = edClassService.getEdClasses(pageSize, pageNumber);
        return ResponseEntity.ok(edClassResponseDTO);
    }

    @GetMapping("/{edClassId}")
    public ResponseEntity<EdClassDTO> getEdClass(@PathVariable(value = "edClassId") Integer id) throws CustomException {
        EdClassDTO e = edClassService.getEdClass(id);
        return ResponseEntity.ok(e);
    }

    @PutMapping("/{edClassId}")
    public ResponseEntity<EdClassDTO> updateEdClass(@PathVariable(value = "edClassId") Integer id,
                                                  @RequestBody Edclass edClass) throws CustomException {
        EdClassDTO e = edClassService.update(id, edClass);
        return ResponseEntity.ok(e);
    }

    @DeleteMapping("/{edClassId}")
    public ResponseEntity<Object> deleteEdClass(@PathVariable(value = "edClassId") Integer id) throws CustomException {
        Boolean b = edClassService.delete(id);
        if (b)
            return ResponseEntity.ok("OK");
        else return ResponseEntity.badRequest().body("Error");
    }

}
