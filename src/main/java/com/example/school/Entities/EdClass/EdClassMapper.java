package com.example.school.Entities.EdClass;

import com.example.school.jooq.tables.pojos.Edclass;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.school.jooq.tables.daos.AcademicyearDao;
import com.example.school.jooq.tables.daos.ParallelDao;
import com.example.school.jooq.tables.daos.TeacherDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EdClassMapper {
    @Autowired
    AcademicyearDao academicYearDao;

    @Autowired
    ParallelDao parallelDao;

    @Autowired
    TeacherDao teacherDao;

    // List<EdClassDTO> -> EdClassResponseDTO
    public EdClassResponseDTO mapToEdClassResponseDTO(List<EdClassDTO> edClassDTOList, Integer currentPage, Integer totalItems) {
        EdClassResponseDTO edClassResponseDTO = new EdClassResponseDTO();
        edClassResponseDTO.setEdClassDTOList(edClassDTOList);
        edClassResponseDTO.setPageCount((int)Math.ceil(totalItems/15.0));
        edClassResponseDTO.setTotalItems(totalItems);
        return edClassResponseDTO;
    }

    // EdClass -> EdClassDTO

    public EdClassDTO mapToEdClassDTO(Edclass edClass) {
        EdClassDTO dto = new EdClassDTO();

        dto.setId(edClass.getId());
        dto.setYearId(edClass.getYearId());
        dto.setPrlId(edClass.getPrlId());
        dto.setTeachId(edClass.getTeachId());
        dto.setAcademicYear(academicYearDao.fetchOneById(edClass.getYearId()));
        dto.setParallel(parallelDao.fetchOneById(edClass.getPrlId()));
        dto.setTeacher(teacherDao.fetchOneById(edClass.getTeachId()));
        return dto;

    }

    // EdClassDTO -> EdClass

    public Edclass mapToEdClass(EdClassDTO edClassDTO) {
        Edclass edClass = new Edclass();
        edClass.setId(edClassDTO.getId());
        edClass.setYearId(edClassDTO.getYearId());
        edClass.setPrlId(edClassDTO.getPrlId());
        edClass.setTeachId(edClassDTO.getTeachId());
        return edClass;
    }

}