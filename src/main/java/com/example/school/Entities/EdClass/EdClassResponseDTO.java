package com.example.school.Entities.EdClass;

import lombok.Data;

import java.util.List;

@Data
public class EdClassResponseDTO {
    private Integer totalItems;
    private Integer pageCount;
    private List<EdClassDTO> edClassDTOList;
}
