package com.example.school.Entities.EdClass;

import com.example.school.jooq.Tables;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.school.jooq.tables.pojos.Edclass;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class EdClassRepository {
    @Autowired
    private final DSLContext dsl;

    private final EdClassMapper edClassMapper;

    public EdClassDTO insert(Edclass edClass) {
        return dsl.insertInto(Tables.EDCLASS)
                .set(dsl.newRecord(Tables.EDCLASS, edClass))
                .returning()
                .fetchOneInto(EdClassDTO.class);
    }

    public EdClassDTO find(Integer id) {
        return dsl.selectFrom(Tables.EDCLASS)
                .where(Tables.EDCLASS.ID.eq(id))
                .fetchOneInto(EdClassDTO.class);
    }

    public List<EdClassDTO> findAll(Integer pageSize, Integer pageNumber) {
        return dsl.selectFrom(Tables.EDCLASS)
                .orderBy(Tables.EDCLASS.ID)
                .limit(pageSize)
                .offset(pageNumber * pageSize)
                .fetchInto(Edclass.class)
                .stream().map(edClassMapper::mapToEdClassDTO)
                .collect(Collectors.toList());
    }

    public Integer getTotalItems() {
        return dsl.selectCount()
                .from(Tables.EDCLASS)
                .fetchOneInto(Integer.class);
    }

    public EdClassDTO update(Integer id, Edclass edClass) {
        return dsl.update(Tables.EDCLASS)
                .set(dsl.newRecord(Tables.EDCLASS, edClass))
                .where(Tables.EDCLASS.ID.eq(id))
                .returning()
                .fetchOneInto(EdClassDTO.class);
    }

    public Boolean delete(Integer id) {
        return dsl.deleteFrom(Tables.EDCLASS)
                .where(Tables.EDCLASS.ID.eq(id))
                .andNotExists(dsl.selectFrom(Tables.STUDENTS).where(Tables.STUDENTS.CLASS_ID.eq(id)))
                .execute() == 1;
    }


}
