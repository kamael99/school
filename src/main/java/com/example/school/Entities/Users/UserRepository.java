package com.example.school.Entities.Users;

import com.example.school.Entities.AcademicYear.AcademicYearDTO;
import com.example.school.jooq.Tables;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.example.school.jooq.tables.pojos.Users;
import java.util.List;

import static org.jooq.impl.DSL.selectFrom;

@Repository
@RequiredArgsConstructor
public class UserRepository {
    @Autowired
    private final DSLContext dsl;

    public UserDTO find(Integer id) {
        return dsl.selectFrom(Tables.USERS)
                .where(Tables.USERS.ID.eq(id))
                .fetchOneInto(UserDTO.class);
    }

    public List<UserDTO> findAll() {
        return dsl.selectFrom(Tables.USERS)
                .orderBy(Tables.USERS.ID)
                .fetch()
                .into(UserDTO.class);
    }

    public UserDTO insert(Users user) {
        return dsl.insertInto(Tables.USERS)
                .set(dsl.newRecord(Tables.USERS, user))
                .returning()
                .fetchOneInto(UserDTO.class);
    }


    public Boolean fetchExists(String login) {
        return dsl
                .fetchExists(
                        selectFrom(Tables.USERS)
                                .where(Tables.USERS.LOGIN.eq(login)
                                        .and(Tables.USERS.DATETIME_OF_DELETE.isNull()))
                );
    }

}
