package com.example.school.Entities.Users;

import lombok.Data;

@Data
public class UserDTO {

    private String login;
    private String password;
    private String name;
    private String surname;
    private String patronymic;
    private Boolean isAdmin;

//    private Integer id;
//
//    private String name;
//
//    private String surname;
//
//    private String patronymic;
}
