package com.example.school.Entities.Users;

import com.example.school.Entities.AcademicYear.AcademicYearDTO;
import com.example.school.Entities.AcademicYear.AcademicYearRepository;
import com.example.school.Exceptions.CustomException;
import com.example.school.jooq.tables.pojos.Students;
import com.example.school.jooq.tables.pojos.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.school.jooq.tables.pojos.Users;

import org.springframework.http.ResponseEntity;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;


    @PostMapping("/new")
    public ResponseEntity<UserDTO> createUser(@RequestBody Users user) throws CustomException {
        UserDTO u = userService.create(user);
        return ResponseEntity.ok(u);
        }

    @GetMapping("/all")
    public List<UserDTO> getAll() {
        return userRepository.findAll();

    }
}
