package com.example.school.Entities.Users;

//import org.springframework.security.crypto.password.PasswordEncoder;
import com.example.school.Entities.Student.StudentResponseDTO;
import com.example.school.Exceptions.CustomException;
import com.example.school.jooq.tables.pojos.Teacher;
import com.example.school.jooq.tables.pojos.Users;
//import org.springframework.security.crypto.password.PasswordEncoder;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.school.Exceptions.ValidationException;
//import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private final PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;


    public UserDTO create(Users user) throws CustomException {
        validateUser(user);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setIsActive(true);
        return userRepository.insert(user);
    }

    private void validateUser(Users user) throws CustomException {
        if(userRepository.fetchExists(user.getLogin())){
            throw new CustomException("This login is already taken!", HttpStatus.BAD_REQUEST);
        }
    }




//
//    private void validateUser(Users user){
//        if(userRepository.fetchExists(user.getLogin())){
//            throw new ValidationException("Такой пользователь уже существует!");
//        }
//    }

}
