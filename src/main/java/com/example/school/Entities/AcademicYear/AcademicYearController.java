package com.example.school.Entities.AcademicYear;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/acyear")
public class AcademicYearController {

    @Autowired
    private AcademicYearService academicYearService;

    @Autowired
    private AcademicYearRepository academicYearRepository;

    @GetMapping("/alll")
    public ResponseEntity<AcademicYearResponseDTO> getAcademicYears(@RequestParam(required = false, defaultValue = "50") Integer pageSize,
                                                                          @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        AcademicYearResponseDTO academicYearResponseDTO = academicYearService.getAcademicYears(pageSize,pageNumber);
        return ResponseEntity.ok(academicYearResponseDTO);
    }

    @GetMapping("/all")
    public List<AcademicYearDTO> getAll() {
        return academicYearRepository.findAlll();

    }


}
