package com.example.school.Entities.AcademicYear;

import lombok.Data;

import java.util.List;

@Data
public class AcademicYearResponseDTO {
    private Integer totalItems;
    private Integer currentPage;

    private List<AcademicYearDTO> academicYearDTOList;
}
