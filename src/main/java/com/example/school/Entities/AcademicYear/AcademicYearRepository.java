package com.example.school.Entities.AcademicYear;

import com.example.school.jooq.Tables;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.school.jooq.tables.pojos.Academicyear;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class AcademicYearRepository {

    @Autowired
    private final DSLContext dsl;

    private final AcademicYearMapper academicYearMapper;

    public AcademicYearDTO find(Integer id) {
        return dsl.selectFrom(Tables.ACADEMICYEAR)
                .where(Tables.ACADEMICYEAR.ID.eq(id))
                .fetchOneInto(AcademicYearDTO.class);
    }

    public List<AcademicYearDTO> findAll(Integer pageSize, Integer pageNumber) {
        return dsl.selectFrom(Tables.ACADEMICYEAR)
                .orderBy(Tables.ACADEMICYEAR.ID)
                .limit(pageSize)
                .offset(pageNumber * pageSize)
                .fetchInto(Academicyear.class)
                .stream().map(academicYearMapper::mapToAcademicYearDTO)
                .collect(Collectors.toList());
    }

    public List<AcademicYearDTO> findAlll() {
        return dsl.selectFrom(Tables.ACADEMICYEAR)
                .orderBy(Tables.ACADEMICYEAR.ID)
                .fetch()
                .into(AcademicYearDTO.class);
    }

    public Integer getTotalItems() {
        return dsl.selectCount()
                .from(Tables.ACADEMICYEAR)
                .fetchOneInto(Integer.class);
    }


}
