package com.example.school.Entities.AcademicYear;

import com.example.school.jooq.tables.pojos.Academicyear;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AcademicYearMapper {


    public AcademicYearResponseDTO mapToAcademicYearResponseDTO(List<AcademicYearDTO> academicYearDTOList, Integer currentPage, Integer totalItems) {
        AcademicYearResponseDTO academicYearResponseDTO = new AcademicYearResponseDTO();
        academicYearResponseDTO.setAcademicYearDTOList(academicYearDTOList);
        academicYearResponseDTO.setCurrentPage(currentPage);
        academicYearResponseDTO.setTotalItems(totalItems);
        return academicYearResponseDTO;
    }

    public AcademicYearDTO mapToAcademicYearDTO(Academicyear academicYear) {
        AcademicYearDTO dto  = new AcademicYearDTO();
        dto.setId(academicYear.getId());
        dto.setTitle(academicYear.getTitle());
        return dto;
    }
}
