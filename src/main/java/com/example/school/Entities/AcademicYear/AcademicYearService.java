package com.example.school.Entities.AcademicYear;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcademicYearService {


    private final Integer defaultPageSize = 50;
    @Autowired
    private AcademicYearMapper academicYearMapper;

    @Autowired
    private AcademicYearRepository academicYearRepository;

    public AcademicYearResponseDTO getAcademicYears(Integer pageSize, Integer pageNumber) {
        if (pageSize > defaultPageSize)
            pageSize = defaultPageSize;

        return academicYearMapper.mapToAcademicYearResponseDTO(academicYearRepository.findAll(pageSize, pageNumber),
                pageNumber,
                academicYearRepository.getTotalItems());
    }


}
