package com.example.school.Entities.AcademicYear;

import lombok.Data;

@Data
public class AcademicYearDTO {
    private Integer id;
    private Integer title;
}
