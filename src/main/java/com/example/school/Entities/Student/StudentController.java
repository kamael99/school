package com.example.school.Entities.Student;


import com.example.school.Exceptions.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.school.jooq.tables.pojos.Students;

@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @PostMapping("/new")
    public ResponseEntity<StudentDTO> createStudent(@RequestBody Students student) throws CustomException {
        StudentDTO s = studentService.create(student);
        return ResponseEntity.ok(s);
    }


    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> getStudent(@PathVariable(value = "id") Integer id) throws CustomException {
        StudentDTO s = studentService.getStudent(id);
        return ResponseEntity.ok(s);
    }

    @GetMapping("/")
    public ResponseEntity<StudentResponseDTO> getStudents(@RequestParam(required = false, defaultValue = "50") Integer pageSize,
                                                          @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        System.out.println(pageSize);
        System.out.println(pageNumber);
        StudentResponseDTO studentResponseDTO = studentService.getStudents(pageSize, pageNumber);
        return ResponseEntity.ok(studentResponseDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<StudentDTO> updateStudent(@PathVariable(value = "id") Integer id,
                                                    @RequestBody Students student) throws CustomException {
        StudentDTO s = studentService.update(id, student);
        return ResponseEntity.ok(s);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deletestudent(@PathVariable(value = "id") Integer id) throws CustomException {
        Boolean b = studentService.delete(id);
        if (b)
            return ResponseEntity.ok("Ok");
        else
            return ResponseEntity.badRequest().body("Error");
    }

    @PutMapping("/transfer/{id}")
    public ResponseEntity<StudentDTO> transferStudent(@PathVariable(value = "id") Integer id,
                                                      @RequestParam(required = true) Integer newClass) throws CustomException {
        StudentDTO s = studentService.transfer(id, newClass);
        return ResponseEntity.ok(s);
    }

}
