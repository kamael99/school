package com.example.school.Entities.Student;

import com.example.school.jooq.Tables;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.example.school.jooq.tables.pojos.Students;
import org.jooq.DSLContext;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class StudentRepository {
    @Autowired
    private final DSLContext dsl;

    private final StudentMapper studentMapper;

    public StudentDTO insert(Students student) {
        return dsl.insertInto(Tables.STUDENTS)
                .set(dsl.newRecord(Tables.STUDENTS, student))
                .returning()
                .fetchOneInto(StudentDTO.class);

    }

    public StudentDTO update(Integer id, Students student) {
        return dsl.update(Tables.STUDENTS)
                .set(dsl.newRecord(Tables.STUDENTS, student))
                .where(Tables.STUDENTS.ID.eq(id))
                .returning()
                .fetchOneInto((StudentDTO.class));
    }

    public StudentDTO find(Integer id) {
        return dsl.selectFrom(Tables.STUDENTS)
                .where(Tables.STUDENTS.ID.eq(id))
                .and(Tables.STUDENTS.DELETE_DATE.isNull())
                .fetchOneInto(StudentDTO.class);
    }

    public List<StudentDTO> findAll(Integer pageSize, Integer pageNumber) {
        return dsl.selectFrom(Tables.STUDENTS)
                .where(Tables.STUDENTS.DELETE_DATE.isNull())
                .orderBy(Tables.STUDENTS.ID)
                .limit(pageSize)
                .offset(pageNumber * pageSize)
                .fetchInto(Students.class)
                .stream().map(studentMapper::mapToStudentDTO)
                .collect(Collectors.toList());
    }

    public Integer getTotalItems() {
        return dsl.selectCount()
                .from(Tables.STUDENTS)
                .where(Tables.STUDENTS.DELETE_DATE.isNull())
                .fetchOneInto(Integer.class);
    }
}
