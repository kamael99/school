package com.example.school.Entities.Student;

import lombok.Data;

import java.util.List;

@Data
public class StudentResponseDTO {
    private Integer totalItems;
    private Integer pageCount;
    private List<StudentDTO> studentDTOList;
}
