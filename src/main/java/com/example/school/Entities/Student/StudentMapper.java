package com.example.school.Entities.Student;

import org.springframework.stereotype.Service;
import com.example.school.jooq.tables.pojos.Students;

import java.util.List;

@Service
public class StudentMapper {

    public StudentDTO mapToStudentDTO(Students student) {
        StudentDTO dto = new StudentDTO();

        dto.setId(student.getId());
        dto.setClassId(student.getClassId());
        dto.setClassId(student.getClassId());
        dto.setDeleteDate(student.getDeleteDate());
        dto.setName(student.getName());
        dto.setSurname(student.getSurname());
        dto.setPatronymic(student.getPatronymic());
        return dto;
    }

    public StudentResponseDTO mapToStudentResponseDTO(List<StudentDTO> studentDTOList, Integer currentPage, Integer totalItems) {
        StudentResponseDTO studentResponseDTO = new StudentResponseDTO();
        studentResponseDTO.setStudentDTOList(studentDTOList);
        //System.out.println(totalItems);
        //System.out.println((int)Math.ceil(totalItems/15.0));
        studentResponseDTO.setPageCount((int)Math.ceil(totalItems/15.0));
        studentResponseDTO.setTotalItems(totalItems);
        return studentResponseDTO;
    }

    public Students mapToStudent(StudentDTO studentDTO) {
        Students student = new Students();
        student.setId(studentDTO.getId());
        student.setClassId(studentDTO.getClassId());
        student.setDeleteDate(studentDTO.getDeleteDate());
        student.setName(studentDTO.getName());
        student.setSurname(studentDTO.getSurname());
        student.setPatronymic(studentDTO.getPatronymic());
        return student;

    }


}
