package com.example.school.Entities.Student;

import com.example.school.Entities.EdClass.EdClassRepository;
import com.example.school.Exceptions.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.school.jooq.tables.pojos.Students;

import java.time.LocalDateTime;
import java.util.Objects;

@Service
public class StudentService {
    private final Integer defaultPageSize = 15;
    private final StudentMapper studentMapper = new StudentMapper();

    @Autowired
    private EdClassRepository edClassRepository;
    @Autowired
    private StudentRepository studentRepository;

    public StudentDTO create(Students student) throws CustomException {
        // Если передали id, то возвращаем ошибку
        if (student.getId() != null)
            throw new CustomException("Id field is prohibited", HttpStatus.BAD_REQUEST);
        // Если передали deleteDate, то возвращаем ошибку
        if (student.getDeleteDate() != null)
            throw new CustomException("Id field is prohibited", HttpStatus.BAD_REQUEST);
        // Пустое имя
        if (student.getName() == null)
            throw new CustomException("Missing required field: name", HttpStatus.BAD_REQUEST);
        // Пустая фамилия, допускаем, что отчества может не быть
        if (student.getSurname() == null)
            throw new CustomException("Missing required field: surname", HttpStatus.BAD_REQUEST);
        // Пустое поле ClassId
        if (student.getClassId() == null)
            throw new CustomException("Missing required field: classId", HttpStatus.BAD_REQUEST);
        // Класс с таким ClassId не существует
        if (edClassRepository.find(student.getClassId()) == null)
            throw new CustomException(String.format("Education Class with id %d not found", student.getClassId()), HttpStatus.BAD_REQUEST);

        return studentRepository.insert(student);
    }

    public StudentDTO update(Integer id, Students student) throws CustomException {
        // Пустое имя
        if (student.getName().isBlank())
            throw new CustomException("Name can't be empty", HttpStatus.BAD_REQUEST);
        // Пустая фамилия
        if (student.getSurname().isBlank())
            throw new CustomException("Surname can't be empty", HttpStatus.BAD_REQUEST);
        // Изменили дату удаления
        if (student.getDeleteDate() != null)
            throw new CustomException("Attempt to change deleteDate", HttpStatus.BAD_REQUEST);

        // Изменили класс на несуществующий
        if (edClassRepository.find(student.getClassId()) == null)
            throw new CustomException("Attempt to change to non existent ClassId", HttpStatus.BAD_REQUEST);
        StudentDTO t = studentRepository.update(id, student);
        // Нет сущности с таким id
        if (t == null)
            throw new CustomException(String.format("Student with id %d not found", id), HttpStatus.NOT_FOUND);

        return t;
    }

    public StudentDTO getStudent(Integer id) throws CustomException {
        StudentDTO t = studentRepository.find(id);
        // Нет сущности с таким id
        if (t == null)
            throw new CustomException(String.format("Student with id %d not found", id), HttpStatus.NOT_FOUND);
        return t;
    }

    public StudentResponseDTO getStudents(Integer pageSize, Integer pageNumber) {
        if (pageSize > defaultPageSize)
            pageSize = defaultPageSize;
        return studentMapper.mapToStudentResponseDTO(studentRepository.findAll(pageSize, pageNumber),
                pageNumber,
                studentRepository.getTotalItems());
    }

    public Boolean delete(Integer id) throws CustomException {

        System.out.println(id);
        StudentDTO t = studentRepository.find(id);
        // Нет сущности с таким id
        if (t == null) {
            throw new CustomException(String.format("Student with id %d not found", id), HttpStatus.NOT_FOUND);
        } else if (t.getDeleteDate() == null) {
            Students s = studentMapper.mapToStudent(t);
            s.setDeleteDate(LocalDateTime.now());
            StudentDTO temp = studentRepository.update(id, s);
            return Boolean.TRUE;
        } else
            // Школьник с таким id уже был удалён (есть поле deleteDate)
            throw new CustomException(String.format("Student with id %d was deleted earlier", id), HttpStatus.NOT_FOUND);
    }


    public StudentDTO transfer(Integer id, Integer newClass) throws CustomException {

        if ((newClass != null) && (newClass < 0)) {
            // Нет такого учебного класса
            if (edClassRepository.find(newClass) == null)
                throw new CustomException(String.format("Education Class with id %d not found", newClass), HttpStatus.BAD_REQUEST);

            StudentDTO t = studentRepository.find(id);
            // Нет сущности с таким id
            if (t == null)
                throw new CustomException(String.format("Student with id %d not found", id), HttpStatus.NOT_FOUND);
            else {
                t.setClassId(newClass);
            }
            return t;
        } else
            // Некорректное значение id класса
            throw new CustomException(String.format("Incorrect edClass %d for transfering the Student", newClass), HttpStatus.NOT_FOUND);
    }
}
