package com.example.school.Entities.Student;

import lombok.Data;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
public class StudentDTO {
    //DTO для класса Student
    private Integer id;
    private Integer classId;
    //private LocalDateTime deleteDate;
    private LocalDateTime deleteDate;
    private String name;
    private String surname;
    private String patronymic;

}

