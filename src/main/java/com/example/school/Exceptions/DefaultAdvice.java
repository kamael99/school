package com.example.school.Exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DefaultAdvice {
    @ExceptionHandler({CustomException.class})
    public ResponseEntity<Response> handleException(CustomException e) {
        Response response = new Response(e.getMessage(), e.getStatus());
        return new ResponseEntity<>(response, e.getStatus());
    }
}
