package com.example.school.Exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class CustomException extends Exception {
    private HttpStatus status;

    public CustomException(String message,HttpStatus httpStatus) {
        super(message);
        this.status = httpStatus;
    }

}
